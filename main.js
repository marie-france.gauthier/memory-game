class game {
    constructor () {
        
        this.cards = [
            { 
                'name' : 'alpha',
                'code' : '\u03b1',
                'color' : "red"
            },
            { 
                'name' : 'alpha',
                'code' : '\u0391',
                'color' : "red"
            },
            { 
                'name' : 'beta',
                'code' : '\u03b2',
                'color' : "blue"
            },
            { 
                'name' : 'beta',
                'code' : '\u0392',
                'color' : "blue"
            },
            { 
                'name' : 'gamma',
                'code' : '\u03b3',
                'color' : "green"
            },
            { 
                'name' : 'gamma',
                'code' : '\u0393',
                'color' : "green"
            },
            { 
                'name' : 'delta',
                'code' : '\u03b4',
                'color' : "crimson"
            },
            { 
                'name' : 'delta',
                'code' : '\u0394',
                'color' : "crimson"
            },
            { 
                'name' : 'epsilon',
                'code' : '\u03b5',
                'color' : "azur"
            },
            { 
                'name' : 'epsilon',
                'code' : '\u0395',
                'color' : "azur"
            },
            { 
                'name' : 'zeta',
                'code' : '\u03b6',
                'color' : "yellow"
            },
            { 
                'name' : 'zeta',
                'code' : '\u0396',
                'color' : "yellow"
            },
            { 
                'name' : 'eta',
                'code' : '\u03b7',
                'color' : "grey"
            },
            { 
                'name' : 'eta',
                'code' : '\u0397',
                'color' : "grey"
            },
            // { 
            //     'name' : 'theta',
            //     'code' : '\u03b8',
            //     'color' : "chocolate"
            // },
            // { 
            //     'name' : 'theta',
            //     'code' : '\u0398',
            //     'color' : "chocolate"
            // },
            // { 
            //     'name' : 'iota',
            //     'code' : '\u03b9',
            //     'color' : "royalblue"
            // },
            // { 
            //     'name' : 'iota',
            //     'code' : '\u0399',
            //     'color' : "royalblue"
            // },
            // { 
            //     'name' : 'kappa',
            //     'code' : '\u03ba',
            //     'color' : "tomato"
            // }, 
            // { 
            //     'name' : 'kappa',
            //     'code' : '\u039a',
            //     'color' : "tomato"
            // },
        ];


        this.deck = document.querySelector('#game');
        this.grid = document.createElement('section');
        this.grid.setAttribute('class', 'grid');
        this.deck.appendChild(this.grid);

        this.count = 0;

        //variables pour le compteur
        this.moves = 0;
        this.counter = document.querySelector('.nbMoves');

        //variables pour le timer
        this.second = 0;
        this.minute = 0;
        this.hour = 0;
        this.timer = document.querySelector('.timer');
        this.timer.innerHTML = "00:00"
        this.interval;

        //variables pour les choix
        this.firstChoice = '';
        this.secondChoice = '';
        this.previousTarget = null;

        //déclaration de la variable por récupérer toutes les cartes qui match
        this.matchedCards = document.getElementsByClassName('match');

        this.cardsGame = this.cards.sort(() => Math.random() - 0.5);
        //this.cardsGame = this.cards[Math.floor(Math.random()*this.cards.length)];
        
        this.cardsGame.forEach(item =>{
            let card = document.createElement('div');
            card.classList.add('card');

            card.dataset.name = item.name;
            //création de la div .front
            let front = document.createElement('div');
            front.classList.add('front');
            front.innerHTML = "❓";
            front.style.fontSize = "80px";
            front.style.paddingTop = "25px";
            //création de la div .back
            let back = document.createElement('div');
            back.classList.add('back');
            
            //Création de l'icone dans la div puis ajout de la class appropriée
            let icon = document.createElement('i');
            //icon.setAttribute('class', 'fas');
            //Ajout de l'icone à la card et ajout de la class _visuelle_
            //icon.classList.add(item.icon);
            icon.style.color = item.color;
            icon.innerHTML = item.code;

            this.grid.appendChild(card);
            card.appendChild(front);
            card.appendChild(back);
            back.appendChild(icon);
        });
    }
    

    match() {
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'match' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('match');
        });
        //Gestion de la victoire
        if (this.matchedCards.length == 14){
            this.congratulation();
        }
    }

    unmatch() {
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Ajout de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.add('unmatch');
        });
    }

    resetChoices() {
        //reset des variables
        this.count = 0;
        this.firstChoice = '';
        this.secondChoice = '';
        //récupération des class 'selected'
        this.selected = document.querySelectorAll('.selected');
        //Suppression de la class 'unmatch' à chacunes d'elles
        this.selected.forEach(card => {
            card.classList.remove('selected', 'unmatch');
        });
    }

    //Méthode affichant la victoire
    congratulation(){
        //récupération du temps total
        let finalTime = this.timer.innerHTML;

        alert(this.moves+' coups en '+finalTime);
        this.playAgain();
    }

    //Méthode pour rejouer
    playAgain() {
        window.location.reload();
    }

    //Méthode du compteur de coups
    startCounter() {
        this.moves++;
        this.counter.innerHTML = this.moves;

        if(this.moves == 1){
            this.startTimer();
        }
    }

    //Méthode du timer
    startTimer() {
        //interval toutes les secondes
        this.interval = setInterval(() =>{
            //Affichage
            this.timer.innerHTML= this.minute+":"+this.second;
            this.second++;
            //Incrémente minute toutes les 60s
            if(this.second == 60){
                this.minute++;
                this.second = 0;
            }
            //Incrémente hour toutes les 60m
            if(this.minute == 60){
                this.hour++;
                this.minute = 0;
            }

        }, 1000);
    }

    start() {
        this.grid.addEventListener('click', event => {
            
            let clicked = event.target;

            if(
                clicked.nodeName === "SECTION" || 
                clicked === this.previousTarget ||
                clicked.parentNode.classList.contains('selected')
            ){
                return;
            }

            if(this.count < 2){
                this.count++;

                //Premier essai
                if (this.count === 1){
                    //récupération du data-name
                    this.firstChoice = clicked.parentNode.dataset.name;
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                    //start le compteur
                    this.startCounter();
                } else if (this.count === 2) {
                    //deuxième essai
                    //récupération du data-name
                    this.secondChoice = clicked.parentNode.dataset.name;
                    //ajout de la class 'selected'
                    clicked.parentNode.classList.add('selected');
                    
                }
                //Si nos choix sont pas vides
                if (this.firstChoice !== '' && this.secondChoice !== ''){

                    //Si les 2 choix sont identiques
                    if (this.firstChoice === this.secondChoice){
                        //Run la fonction match
                        this.match();
                        //Run la fonction resetChoices
                        this.resetChoices();
                    //Sinon
                    } else  {
                        
                        //Run la fonction unmatch
                        this.unmatch();

                        setTimeout(() => {
                            this.resetChoices();
                        }, 1000);                      
                        //setTimeout(this.resetChoices, 1000);
                    	//this.resetChoices();
                    }
                }
                //set du previousTarget
                this.previousTarget = clicked;
            }
            
        });
    }
}
let letsPlay = new game;
letsPlay.start(); 